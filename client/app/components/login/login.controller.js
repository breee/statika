class LoginController {
  constructor ($state, LoginService) {
    this.$state = $state
    this.loginService = LoginService
    this.loginInProgress = false
    this.message = 'Sign in'
  }

  login (username, password) {
    console.log(this.username, this.password)
    this.loginInProgress = true
    this.loginService.login(this.username, this.password).then(
      
      () => {
        this.loginInProgress = false
        this.$state.go('home')
      },
      (error) => {
        this.loginInProgress = false
      }
    )
  }
}

LoginController.$inject = ['$state', 'LoginService']

export default LoginController
