class LoginCtrl {
  constructor ($state, LoginService) {
    this.$state = $state;
    this.loginService = LoginService;

    this.loginInProgress = false;
  }

  login () {
    this.loginInProgress = true;
    this.loginService.login(this.email, this.password).then(
      () => {
        this.loginInProgress = false;
        $state.transitionTo('home');
      },
      (error) => {
        this.loginInProgress = false
      }
    )
  }
}

LoginCtrl.$inject = ['$state', 'LoginService'];

export default LoginCtrl;
