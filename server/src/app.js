'use strict'

//Environment modules
import dotenv          from 'dotenv'
import express         from 'express'
import keypair         from 'keypair'
import sysLogger       from 'simple-node-logger'
import path            from 'path'
import logger          from 'morgan'
import jwt             from 'express-jwt'
import mongoose        from 'mongoose'
import bodyParser      from 'body-parser'
import JWTRedisSession from 'jwt-redis-session-extra'
//Database configuration module
import databaseServer  from './config/db'
//Auth modules
import keychains       from './authkey'

//Enviroment configuration variables
dotenv.config()

//Database Nodejs clients
const { mongoClient, redisClient, secretToken } = databaseServer

//MongoDB config
mongoose.connect(mongoClient, {
  useMongoClient: true
})
      
//Auth keys for server api access request
const { pub, priv } = keychains

//Express module instances
const app       = express(),
      jwtRouter = express.Router()

//static path config
app.use(express.static(path.join(__dirname, '../../dist')))

//logger config
app.use(logger('dev'))

//bodyparser config
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')


//JWT redis session config
app.use(JWTRedisSession({
	client    : redisClient,
	passphrase: secretToken,
	pubkey    : pub,
	secret    : priv,
	keyspace  : "sess:",
	maxAge    : 1800,
  algorithm : "RS256",
  requestKey: "jwtSession",
  requestArg: "jwtToken"
}))

app.all('*', function(req, res, next) {
	res.set('Access-Control-Allow-Origin', 'http://localhost')
	res.set('Access-Control-Allow-Credentials', true)
	res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT')
	res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, x-jwt-token')
	if ('OPTIONS' == req.method) return res.sendStatus(200)
	next()
})

app.use('/admin', jwtRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found')
	err.status = 404
	next(err)
})

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    })
  })

  app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
      res.send(401, 'invalid token...')
    }
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

export default app 
