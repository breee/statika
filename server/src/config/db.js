'use strict'

//Database modules
import redis     from 'redis'
import mongoose  from 'mongoose'
import sysLogger from "simple-node-logger"

const redisClient = redis.createClient(),
      mongodb     = mongoose.connection,
      opts        = { logFilePath:'sys.log', timestampFormat:'MMM.DD.YYYY HH:MM A' },
      log         = sysLogger.createSimpleLogger(opts)

redisClient.on("error", function (err) {
  log.info('Redis Error ', err,' occurred at ', new Date().toJSON())
})
redisClient.on('connect', function () {
	log.info('Redis initialized at ', new Date().toJSON())
})

mongodb.on('error', console.error.bind(console, 'Connection error: '))
mongodb.once('open', function() {
	log.info('MongoDB initialized at ', new Date().toJSON())
});

module.exports = {
	secretToken : '137BAB574DRL6AA6A8MDD',
	redisClient : redisClient,
	mongoClient	: 'mongodb://localhost:27017/statika'
}
