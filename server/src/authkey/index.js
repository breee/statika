'use strict'

import fse  from 'fs-extra'
import path from 'path'

module.exports = {
  pub  : fse.readFileSync(path.join(__dirname, 'pub.pem')),
  priv : fse.readFileSync(path.join(__dirname, 'pub.pem'))
}